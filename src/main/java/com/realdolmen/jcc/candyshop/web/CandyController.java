package com.realdolmen.jcc.candyshop.web;

import com.realdolmen.jcc.candyshop.domain.Candy;
import com.realdolmen.jcc.candyshop.domain.CandyColor;
import com.realdolmen.jcc.candyshop.services.CandyService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("candy")
public class CandyController {

    @Inject
    private Logger logger;

    @Inject
    private CandyService candyService;

    @GET
    @Produces("application/json")
    public List<Candy> findAllCandy() {
        logger.log(Level.ALL, "Hello?");
        return candyService.getAllCandy();
    }

    @GET
    @Path("/test")
    public String ping() {
        return "pong";
    }

    @GET
    @Path("{color}")
    public List<Candy> findCandyByColor(CandyColor candyColor) {
        throw new UnsupportedOperationException();
    }
}
