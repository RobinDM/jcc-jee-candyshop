package com.realdolmen.jcc.candyshop.web;

import com.realdolmen.jcc.candyshop.domain.Person;
import com.realdolmen.jcc.candyshop.services.PersonService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class PersonController {
    @Inject
    private PersonService personService;

    void savePerson(){
        throw new UnsupportedOperationException();
    }

    Person findPersonById(){
        throw new UnsupportedOperationException();
    }
}
