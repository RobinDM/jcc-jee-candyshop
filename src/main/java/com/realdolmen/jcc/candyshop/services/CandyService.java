package com.realdolmen.jcc.candyshop.services;

import com.realdolmen.jcc.candyshop.domain.Candy;
import com.realdolmen.jcc.candyshop.repositories.jdbc.JdbcCandyRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class CandyService {
    @Inject
    private JdbcCandyRepository candyRepository;

    @Inject
    private PersonService personService;

    public List<Candy> getAllCandy() {
        return candyRepository.getAllCandy();
    }
}
