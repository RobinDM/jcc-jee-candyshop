package com.realdolmen.jcc.candyshop;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api")
public class App extends Application {

//    @Override
//    public Set<Class<?>> getClasses() {
//        final Set<Class<?>> returnValue = new HashSet<>();
//        returnValue.add(CandyController.class);
//        return returnValue;
//    }
}
