package com.realdolmen.jcc.candyshop.domain;

import java.io.Serializable;
import java.util.Objects;

public class Candy implements Serializable {
    private String name;

    public Candy(){ }

    public Candy(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Candy candy = (Candy) o;
        return Objects.equals(name, candy.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Candy{" + "name='" + name + '\'' + '}';
    }
}
