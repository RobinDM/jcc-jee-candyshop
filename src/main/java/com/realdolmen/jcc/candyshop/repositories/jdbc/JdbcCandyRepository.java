package com.realdolmen.jcc.candyshop.repositories.jdbc;

import com.realdolmen.jcc.candyshop.domain.Candy;
import com.realdolmen.jcc.candyshop.repositories.CandyRepository;

import javax.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class JdbcCandyRepository implements CandyRepository {

    public JdbcCandyRepository(){
        System.out.println("Creating JDBC candy repository");
    }

    @Override
    public List<Candy> getAllCandy() {
        return Arrays.asList(new Candy("Snickers"), new Candy("Mars"));
    }
}
