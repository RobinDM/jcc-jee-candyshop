package com.realdolmen.jcc.candyshop.repositories;

import com.realdolmen.jcc.candyshop.domain.Candy;

import java.util.List;

public interface CandyRepository {

    List<Candy> getAllCandy();
}
