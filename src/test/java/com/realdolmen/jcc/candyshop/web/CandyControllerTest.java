package com.realdolmen.jcc.candyshop.web;

import com.realdolmen.jcc.candyshop.domain.Candy;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CandyControllerTest extends AbstractWeldContainerTest {

    @Test
    public void findAllCandyTest() {
        CandyController candyController = container.instance()
                                                   .select(CandyController.class)
                                                   .get();
        List<Candy> allCandy = candyController.findAllCandy();
        Assert.assertEquals(2, allCandy.size());

    }

    @Test
    public void findCandyByColor() {
        Assert.fail("Not yet implemented");
    }
}