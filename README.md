This repository is supposed to be a stub to the JEE exercise on CDI around slide 33.
It is *not* fully implemented, because it took me over 3 hours to get it to run.
Turns out you can't run it on Tomcat8 because it doesn't support JAX-RS, the part of JEE that enables REST endpoints.
Apparently it's possible to do so anyway by adding another dependency to [jersey](https://eclipse-ee4j.github.io/jersey/).

Alternatively, you should use an application server that implements the JEE spec (not just the servlet spec), like [wildfly](https://www.wildfly.org/) (formerly: JBoss AS).

This example finally deploys to WildFly (standalone).

I can't find where to set the context url, so it's deployed to something ridiculous like http://localhost:8080/jee7candy-1.0-SNAPSHOT.
The `@ApplicationPath` is `api`, and the only implemented REST endpoints are `candy` (which returns a JSON list of `Candy` objects), `candy/test`, which returns `"pong"`, and `candy/{color}`, which will throw an Exception.
